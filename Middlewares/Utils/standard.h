/*
 * @Author: your name
 * @Date: 2021-05-01 19:50:44
 * @LastEditTime: 2021-05-06 18:56:28
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \gcxz\Middlewares\standard.h
 */
#ifndef _STANDARD_H
#define _STANDARD_H

#define LF 0
#define RF 1
#define RB 2
#define LB 3

#define X 0
#define Y 1
#define Z 2

struct Vector3{
    float x;
    float y;
    float z;

    Vector3& operator = (Vector3& value){
        x = value.x; y = value.y; z = value.z;
        return *this;
    }

    Vector3& operator + (Vector3& value){
        x += value.x;
        y += value.y;
        z += value.z;
        return *this;
    }
};


#endif
