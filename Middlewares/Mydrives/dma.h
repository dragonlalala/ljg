#ifndef _DMA_H
#define _DMA_H
#include "include_lib.h"

void DMA_Count_Init(DMA_CHn CHn, PTX_n ptxn, uint32 count, uint32 cfg);
uint32 DMA_Count_Get(DMA_CHn CHn);
void DMA_Count_Reset(DMA_CHn CHn);

#endif

