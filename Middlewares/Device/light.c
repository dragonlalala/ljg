#include "light.h"

void Light_Init()
{
		gpio_init(GREEN_PIN,GPO,1);  //初始化不亮
		gpio_init(RED_PIN,GPO,1);
}
void Chengchi_green()
{
		gpio_turn(GREEN_PIN);//1
		systick_delay_ms(200);
		gpio_turn(GREEN_PIN);
		systick_delay_ms(200);
		gpio_turn(GREEN_PIN);//2
		systick_delay_ms(200);
		gpio_turn(GREEN_PIN);
		systick_delay_ms(200);
		gpio_turn(GREEN_PIN);//3
		systick_delay_ms(200);
		gpio_turn(GREEN_PIN);
		systick_delay_ms(200);
		gpio_init(GREEN_PIN,GPO,1);
}


void Suying_Red()
{
		gpio_turn(RED_PIN);//1
		systick_delay_ms(200);
		gpio_turn(RED_PIN);
		systick_delay_ms(200);
		gpio_turn(RED_PIN);//2
		systick_delay_ms(200);
		gpio_turn(RED_PIN);
		systick_delay_ms(200);
		gpio_turn(RED_PIN);//3
		systick_delay_ms(200);
		gpio_turn(RED_PIN);
		systick_delay_ms(200);
		gpio_init(RED_PIN,GPO,1);
}


void Bee_Init()
{
	gpio_init(BEE_PIN,GPO,0);
}
void Bee(uint8 a,int time)
{			
		
		if(a == 1)
		{
				gpio_set(BEE_PIN,1);
				systick_delay_ms(time);
				gpio_set(BEE_PIN,0);
		}
		else
		{
				gpio_set(BEE_PIN,0);
		}
			
}

