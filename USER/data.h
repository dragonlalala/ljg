/*
 * @Author: your name
 * @Date: 2021-05-02 12:42:04
 * @LastEditTime: 2021-05-06 16:34:52
 * @LastEditors: Please set LastEditors
 * @Description: 放置所有的全局变量
 * @FilePath: \gcxz\USER\data.h
 */
#ifndef _DATA_H
#define _DATA_H

#include "include_mid.h"

#define BASE_SPEED 0

extern uint32 sys_time_ms;

// 任务执行标志位
extern int8 control_flag;		// 控制任务
extern int8 edgeBoard_flag;	    // edgeBoard通信，包括发送与接收
extern int8 upMonitor_flag;	    // 上位机通信，包括发送与接收
extern int8 init_device_flag;   // 初始化舵机举旗等的初始位置
extern int8 debug_flag;		    // 调试任务，包括OLED显示、key设置等
extern int8 couandstop_flag;	// 是否处于做任务状态,包括计步，各地标的任务等
extern int8 task_flag;          // 下位机做任务的标志位
extern int8 adjust_flag;	    // 下位机进行调整的标志位
extern uint8 task_type;         // 下位机接收到的任务类型
extern int8 permit_con_flag;	// 上位机控制要不要下位机进行底盘电机控制
extern uint8 argument_value ;// 上位机发来的要修改的哪个变量的值
extern uint8 which_argument ;// 上位机发来的要修改的哪个变量
extern Chassis chassis;
extern short which_city;        // 用来接收上位机发来的是哪个城池
extern int LB_cnt;
extern int RB_cnt;
extern int8 eb_dubug_flag;  // 接收上位机传来的要改变下位机变量的标志位
#endif
