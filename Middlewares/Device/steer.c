#include "steer.h"

void Steer_Init()
{
	ftm_pwm_init(ftm0,ftm_ch0,50,0);	
	ftm_pwm_init(ftm0,ftm_ch1,50,0);
	ftm_pwm_init(ftm0,ftm_ch2,50,0);	
	ftm_pwm_init(ftm0,ftm_ch3,50,0);
	ftm_pwm_init(ftm0,ftm_ch4,50,0);
	ftm_pwm_init(ftm0,ftm_ch5,50,0);	
	ftm_pwm_init(ftm0,ftm_ch6,50,0);	
	ftm_pwm_init(ftm0,ftm_ch7,50,0);
    set_init_duty();
}
/* 精度为2000的情况下，50是0度，100是45度，150是90度,200是135度，250是180度
增加delay是为了不让所有舵机同时工作避免电流过大*/

void set_init_duty()
{
	//这三个是举旗的
	ftm_pwm_duty(ftm0,ftm_ch0,45);
	systick_delay_ms(10);
	ftm_pwm_duty(ftm0,ftm_ch1,45);
	systick_delay_ms(10);
	ftm_pwm_duty(ftm0,ftm_ch2,45);
	systick_delay_ms(10);
	//这个是打靶的舵机,140即可实现效果
	ftm_pwm_duty(ftm0,ftm_ch3,150);
	systick_delay_ms(10);
	//这是夹方块的肘关节（底下）初始150，终止230
	ftm_pwm_duty(ftm0,ftm_ch4,140);
	systick_delay_ms(10);
	//这是夹方块的爪关节（爪子）张开250，夹住是115
	ftm_pwm_duty(ftm0,ftm_ch5,150);
	systick_delay_ms(10);
	//这是放小球的舵机，
	ftm_pwm_duty(ftm0,ftm_ch6,145);
	systick_delay_ms(10);

	
}

void initFlag()
{
	ftm_pwm_duty(ftm0,ftm_ch0,145);
	systick_delay_ms(5);
	ftm_pwm_duty(ftm0,ftm_ch1,145);
	systick_delay_ms(10);
	ftm_pwm_duty(ftm0,ftm_ch2,145);
	systick_delay_ms(10);
}
/* 举旗的函数，顺序依据实际的城池顺序临时修改*/
void raiseFlags(short a)
{
	if(a == 1)
	{
		ftm_pwm_duty(ftm0,ftm_ch1,45);//涓嶈闈犺繎90搴︼紝浼氭姈
		ftm_pwm_duty(ftm0,ftm_ch2,45);
	}
	else if(a == 2)
	{
		ftm_pwm_duty(ftm0,ftm_ch0,45);
		ftm_pwm_duty(ftm0,ftm_ch2,45);
	}
	else if(a == 3)
	{
		ftm_pwm_duty(ftm0,ftm_ch0,45);
		ftm_pwm_duty(ftm0,ftm_ch1,45);
	}
}
//放棋子
void putDownFlags(short a){
	ftm_pwm_duty(ftm0,ftm_ch0,145);
	systick_delay_ms(10);
    ftm_pwm_duty(ftm0,ftm_ch1,145);
	systick_delay_ms(10);
    ftm_pwm_duty(ftm0,ftm_ch2,145);	
	systick_delay_ms(10);
}

/**
* @brief  夹方块
* @param  None.
* @return None.
*/
void clipBox(){
	ftm_pwm_duty(ftm0,ftm_ch5,250);//爪关节张开
	systick_delay_ms(100);
	ftm_pwm_duty(ftm0,ftm_ch4,150);//肘关节初始化位置
	systick_delay_ms(500);
	ftm_pwm_duty(ftm0,ftm_ch4,230);//肘关节放下去
	systick_delay_ms(2000);
	ftm_pwm_duty(ftm0,ftm_ch5,115);//爪关节夹住
	systick_delay_ms(1000);
	ftm_pwm_duty(ftm0,ftm_ch4,150);//肘关节上来回到初始化位置
	systick_delay_ms(1000);
}


/**
* @brief  倒小球
* @param  None.
* @return None.
*/
void throwBall(){
	ftm_pwm_duty(ftm0,ftm_ch6,65); //TODO:鍙傛暟寰呮敼
	systick_delay_ms(3000);
	ftm_pwm_duty(ftm0,ftm_ch6,145);
}

/**
* @brief  打靶
* @param  None.
* @return None.
*/
void shootTarget(){
	ftm_pwm_duty(ftm0,ftm_ch3,170);
	systick_delay_ms(1600);                       //3绉掞紵TODO:
	ftm_pwm_duty(ftm0,ftm_ch3,150);
}	

