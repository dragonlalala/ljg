/*
 * @Author: your name
 * @Date: 2021-05-02 12:27:13
 * @LastEditTime: 2021-05-06 16:26:59
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \gcxz\USER\init.h
 */
#ifndef _CONFIG_H
#define _CONFIG_H

#include "data.h"
#include "task.h"

void initAll();     // 初始化所有资源，main中调用
void initDevice();
#endif
