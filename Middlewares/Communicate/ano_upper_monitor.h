#ifndef ANO_UPPER_MONITOR_H
#define ANO_UPPER_MONITOR_H



/************************************************************************************************************
 * @文件       		ano_upper_monitor.h
 * @简介      		匿名上位机配套函数库：通过串口发送数据给电脑上位机
 * @组织	   		Scut_SmartCar
 * @作者     		xjn(qq765760800)
 * @发布日期       	2021-01-08
 * @注意事项		1、发送给上位机的数据不能是浮点型，想发送浮点型数据的话乘以10的整数倍化为整形再发送。
					2、匿名上位机软件可以搜官方Q群“ANO匿名开源飞控”获得，上位机的用法可以直接百度。
					3、在实际应用中（特别是当使用的是无线串口时），为保持发送稳定性，建议在发送数据的代码后进行适当时间的延时。
 * @用法			将宏定义修改区中的宏定义修改为自己适用的，
					然后在主函数中直接调用UART_SendData1等函数即可。
 * @用法举例		
					UART_SendData2(ADC_LH, ADC_RH);
					systick_delay_ms(50);
					
					
	
************************************************************************************************************/




//-------------------------------------------宏定义修改区Begin-------------------------------------------//


//这个头文件用于给出数据类型声明，如“	typedef unsigned char       uint8;	” 
#include "common.h"


//串口发送字符数组函数
#define __UART_PUT_CHAR_ARRAY(__CHAR_ARRAY__, __ARRRY_LEN__)					\
	do{																			\
		uart_putbuff (uart2, (uint8 *) __CHAR_ARRAY__, (uint32) __ARRRY_LEN__);	\
	}while(0)
	

	

//--------------------------------------------宏定义修改区End--------------------------------------------//



	
	
	
	

	
//数据拆分宏定义，在发送大于1字节的数据类型时，比如int16、float等，需要把数据拆分成单独字节进行发送
#define BYTE0(dwTemp)       ( *(  (char *)(&dwTemp)     ) )//32位整型数据的最低8位
#define BYTE1(dwTemp)       ( *(  (char *)(&dwTemp) + 1 ) )//32位整型数据的次低8位
#define BYTE2(dwTemp)       ( *(  (char *)(&dwTemp) + 2 ) )//32位整型数据的次高8位
#define BYTE3(dwTemp)       ( *(  (char *)(&dwTemp) + 3 ) )//32位整型数据的最高8位
	
	
//联合体,float->string
union float_string{  
        uint8 float_array[4];  
        float f;  
};


//联合体,int->string
union int_string{  
        uint8 int_array[4];  
        int32 in_t; 
};
extern uint8 bluetooth_flag;
extern uint8 uart_flag;                             	//用来指示是否接收到包头
extern uint8 uart_mode;                                //识别命令
extern uint8 uart_receive_temp[7];                     //暂存uart0接收的数据，待确认接收到包尾后再取出 
extern uint8 uart_account;
extern uint8 uart_datatosend[60];
extern uint8 bluetooth_flag;


//------------以下为用到的函数-------------
void UART_SendData1(int32 datatosend);//发送1个32位整型数据
void UART_SendData2(int32 datatosend1,int32 datatosend2);//发送2个32位整型数据
void UART_SendData3(int32 datatosend1,int32 datatosend2,int32 datatosend3);//发送3个32位整型数据
void UART_SendData4(int32 datatosend1,int32 datatosend2,int32 datatosend3,int32 datatosend4);//发送4个32位整型数据
void UART_SendData4float(float datatosend1,float datatosend2,float datatosend3,float datatosend4);//发送4个32位整型数据
//可以参考这4个函数构造多达10个参数的发送函数


//------------以下为用不到的函数-------------
void Data_Handler(void);
void Check_Handler(void);
void Data_Receive_Prepare(uint8 data);				//接收预处理
void Data_Receive_Handler(uint8 *data_buf,uint8 num);	//对接收的数据处理


#endif

