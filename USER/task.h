/*
 * @Author: your name
 * @Date: 2021-05-02 12:12:29
 * @LastEditTime: 2021-05-06 16:34:13
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \gcxz\USER\task.h
 */
#ifndef _TASK_H
#define _TASK_H

#include "data.h"
#include "include_mid.h"
#include "include_lib.h"
extern uint8 show_which;    // 根据这个来实现oled翻页
extern float copy_chassis_target_x;
void controlTask(void);
void edgeBoardTask(void);
void upMonitorTask(void);
void debugTask(void);
void pingyi(int time);

#endif
