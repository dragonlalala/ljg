#ifndef _STEP_MOTOE_H
#define _STEP_MOTOE_H
#include "include_lib.h"

void Step_Motor_Init(void);
void Step_Motor_Stop(void);
void Run_x_Loop(int x);
#endif
