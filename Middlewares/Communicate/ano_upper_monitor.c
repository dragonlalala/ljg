#include "ano_upper_monitor.h"




uint8 uart_flag=0x00;                             //用来指示是否接收到包头
uint8 uart_mode=0;                                //识别命令
uint8 uart_receive_temp[7];                       //暂存uart4接收的数据，待确认接收到包尾后再取出 
uint8 uart_account=0x00;

uint8 uart_datatosend[60];//主要关注这个数组
uint8 bluetooth_flag;

//-------------------------------------------------------------------------------------------------------------------
//  @brief      向上位机发送1个32位整型数据
//  @param      datatosend			32位整型数据
//  @return     void
//  @since      v1.0
//  Sample usage:			
//-------------------------------------------------------------------------------------------------------------------
void UART_SendData1(int32 datatosend)
{
	uint8 _cnt=0;
	uint8 sum = 0;
	uint8 i=0;
	uart_datatosend[_cnt++]=0xAA;//第1个字节：默认通用数据报头
	//uart_datatosend[_cnt++]=0x05;//第2个字节：发送设备辨识功能码，0x05为匿名拓空者飞控，这个不允许修改！
	//由于上位机软件的bug（或者是故意为之），只要这个功能码不为0x05的数据包，上位机通通都会报错。
									
	uart_datatosend[_cnt++]=0xAA;//0xAF;//第3个字节：目标设备辨识功能码，0xAF为上位机，不用修改。
	uart_datatosend[_cnt++]=0xF1;//第4个字节：第一个字节：用于设置上位机的“高级收码”功能中的数据帧，一般不用改
	uart_datatosend[_cnt++]=4;	//第5个字节：用于存放数据内容长度（注意不是整个数据报的长度）。这一句代码其实就相当于uart_datatosend[4]=0。
	//因为在后面的代码中会重新设置这一个字节，所以先设为0也没关系。
	
	//拆分缓存：将一个32位整型值拆成4个8位的整型值
	uart_datatosend[_cnt++]=BYTE3(datatosend);//第6个字节：32位整型数据的最高8位
	uart_datatosend[_cnt++]=BYTE2(datatosend);//第7个字节：32位整型数据的最次8位
	uart_datatosend[_cnt++]=BYTE1(datatosend);//第8个字节：32位整型数据的最次8位
	uart_datatosend[_cnt++]=BYTE0(datatosend);//第9个字节：32位整型数据的最低8位
	
	//uart_datatosend[3]=_cnt-5;	//重新设置第5个字节。设为数据内容长度。
		
	
	for(;i<_cnt;i++)
		sum += uart_datatosend[i];//计算校验数据位，原理不需要弄懂
	uart_datatosend[_cnt++]=sum;		//设置校验数据位

	__UART_PUT_CHAR_ARRAY(uart_datatosend, _cnt);//串口发送数据给上位机
  
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      向上位机发送2个32位整型数据
//  @param      datatosend1			第1个32位整型数据
//  @param      datatosend2			第2个32位整型数据
//  @return     void
//  @since      v1.0
//  Sample usage:			
//-------------------------------------------------------------------------------------------------------------------
void UART_SendData2(int32 datatosend1,int32 datatosend2)
{
	uint8 _cnt=0;
	uint8 sum = 0;
	uint8 i=0;
	uart_datatosend[_cnt++]=0xAA;
	//uart_datatosend[_cnt++]=0x05;
	uart_datatosend[_cnt++]=0xAA;
	uart_datatosend[_cnt++]=0xF1;
	uart_datatosend[_cnt++]=4;
	
	uart_datatosend[_cnt++]=BYTE3(datatosend1);	//第1个数据拆分缓存
	uart_datatosend[_cnt++]=BYTE2(datatosend1);
	uart_datatosend[_cnt++]=BYTE1(datatosend1);
	uart_datatosend[_cnt++]=BYTE0(datatosend1);	
	
	uart_datatosend[_cnt++]=BYTE3(datatosend2);	//第2个数据拆分缓存
	uart_datatosend[_cnt++]=BYTE2(datatosend2);
	uart_datatosend[_cnt++]=BYTE1(datatosend2);
	uart_datatosend[_cnt++]=BYTE0(datatosend2);
	
		//uart_datatosend[4]=_cnt-5;
		
	
	for(;i<_cnt;i++)
		sum += uart_datatosend[i];
	uart_datatosend[_cnt++]=sum;		//校验数据位
	
	__UART_PUT_CHAR_ARRAY(uart_datatosend, _cnt);//串口发送数据给上位机
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      向上位机发送3个32位整型数据
//  @param      datatosend1			第1个32位整型数据
//  @param      datatosend2			第2个32位整型数据
//  @param      datatosend3			第3个32位整型数据
//  @return     void
//  @since      v1.0
//  Sample usage:			
//-------------------------------------------------------------------------------------------------------------------
void UART_SendData3(int32 datatosend1,int32 datatosend2,int32 datatosend3)
{
	uint8 _cnt=0;
	uint8 sum = 0;
	uint8 i=0;
	uart_datatosend[_cnt++]=0xAA;
	//uart_datatosend[_cnt++]=0x05;
	uart_datatosend[_cnt++]=0xAA;
	uart_datatosend[_cnt++]=0xF1;
	uart_datatosend[_cnt++]=16;
	
	uart_datatosend[_cnt++]=BYTE3(datatosend1);
	uart_datatosend[_cnt++]=BYTE2(datatosend1);
	uart_datatosend[_cnt++]=BYTE1(datatosend1);
	uart_datatosend[_cnt++]=BYTE0(datatosend1);	
	
	uart_datatosend[_cnt++]=BYTE3(datatosend2);
	uart_datatosend[_cnt++]=BYTE2(datatosend2);
	uart_datatosend[_cnt++]=BYTE1(datatosend2);
	uart_datatosend[_cnt++]=BYTE0(datatosend2);	
	
	uart_datatosend[_cnt++]=BYTE3(datatosend3);
	uart_datatosend[_cnt++]=BYTE2(datatosend3);
	uart_datatosend[_cnt++]=BYTE1(datatosend3);
	uart_datatosend[_cnt++]=BYTE0(datatosend3);
		
	
	for(;i<_cnt;i++)
		sum += uart_datatosend[i];
	uart_datatosend[_cnt++]=sum;		//校验数据位

	__UART_PUT_CHAR_ARRAY(uart_datatosend, _cnt);//串口发送数据给上位机
	
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      向上位机发送4个32位整型数据
//  @param      datatosend1			第1个32位整型数据
//  @param      datatosend2			第2个32位整型数据
//  @param      datatosend3			第3个32位整型数据
//  @param      datatosend4			第4个32位整型数据
//  @return     void
//  @since      v1.0
//  Sample usage:			
//-------------------------------------------------------------------------------------------------------------------
void UART_SendData4(int32 datatosend1,int32 datatosend2,int32 datatosend3,int32 datatosend4)
{
	uint8 _cnt=0;
	uint8 sum = 0;
	uint8 i=0;
	uart_datatosend[_cnt++]=0xAA;
	//uart_datatosend[_cnt++]=0x05;
	uart_datatosend[_cnt++]=0xAA;
	uart_datatosend[_cnt++]=0xF1;
	uart_datatosend[_cnt++]=16;
	
	uart_datatosend[_cnt++]=BYTE3(datatosend1);
	uart_datatosend[_cnt++]=BYTE2(datatosend1);
	uart_datatosend[_cnt++]=BYTE1(datatosend1);
	uart_datatosend[_cnt++]=BYTE0(datatosend1);	
	
	uart_datatosend[_cnt++]=BYTE3(datatosend2);
	uart_datatosend[_cnt++]=BYTE2(datatosend2);
	uart_datatosend[_cnt++]=BYTE1(datatosend2);
	uart_datatosend[_cnt++]=BYTE0(datatosend2);
		
	uart_datatosend[_cnt++]=BYTE3(datatosend3);
	uart_datatosend[_cnt++]=BYTE2(datatosend3);
	uart_datatosend[_cnt++]=BYTE1(datatosend3);
	uart_datatosend[_cnt++]=BYTE0(datatosend3);
		
	uart_datatosend[_cnt++]=BYTE3(datatosend4);
	uart_datatosend[_cnt++]=BYTE2(datatosend4);
	uart_datatosend[_cnt++]=BYTE1(datatosend4);
	uart_datatosend[_cnt++]=BYTE0(datatosend4);
		
	
	for(;i<_cnt;i++)
		sum += uart_datatosend[i];
	uart_datatosend[_cnt++]=sum;		//校验数据位
	
	__UART_PUT_CHAR_ARRAY(uart_datatosend, _cnt);//串口发送数据给上位机
	
}



//-------------------------------------------------------------------------------------------------------------------
//  @brief      向上位机发送4个浮点数据
//  @param      datatosend1			第1个浮点数据
//  @param      datatosend2			第2个浮点数据
//  @param      datatosend3			第3个浮点数据
//  @param      datatosend4			第4个浮点数据
//  @return     void
//  @since      v1.0
//  Sample usage:			
//-------------------------------------------------------------------------------------------------------------------
void UART_SendData4float(float datatosend1,float datatosend2,float datatosend3,float datatosend4)
{
	uint8 _cnt=0;
	uint8 sum = 0;
	uint8 i=0;
	uart_datatosend[_cnt++]=0xAA;
	//uart_datatosend[_cnt++]=0x05;
	uart_datatosend[_cnt++]=0xAA;
	uart_datatosend[_cnt++]=0xF1;
	uart_datatosend[_cnt++]=4;
	
	uart_datatosend[_cnt++]=BYTE3(datatosend1);
	uart_datatosend[_cnt++]=BYTE2(datatosend1);
	uart_datatosend[_cnt++]=BYTE1(datatosend1);
	uart_datatosend[_cnt++]=BYTE0(datatosend1);	
	
	uart_datatosend[_cnt++]=BYTE3(datatosend2);
	uart_datatosend[_cnt++]=BYTE2(datatosend2);
	uart_datatosend[_cnt++]=BYTE1(datatosend2);
	uart_datatosend[_cnt++]=BYTE0(datatosend2);
		
	uart_datatosend[_cnt++]=BYTE3(datatosend3);
	uart_datatosend[_cnt++]=BYTE2(datatosend3);
	uart_datatosend[_cnt++]=BYTE1(datatosend3);
	uart_datatosend[_cnt++]=BYTE0(datatosend3);
		
	uart_datatosend[_cnt++]=BYTE3(datatosend4);
	uart_datatosend[_cnt++]=BYTE2(datatosend4);
	uart_datatosend[_cnt++]=BYTE1(datatosend4);
	uart_datatosend[_cnt++]=BYTE0(datatosend4);
		
	
	for(;i<_cnt;i++)
		sum += uart_datatosend[i];
	uart_datatosend[_cnt++]=sum;		//校验数据位
	
	__UART_PUT_CHAR_ARRAY(uart_datatosend, _cnt);//串口发送数据给上位机
	
}

//------------------------------------------------------以下函数均为用不到的函数-----------------------------------------------------------//




/*************************************************************
//							uart中断服务函数
//				
//		函数说明：为蓝牙接收功能
//
**************************************************************/

//void Data_uart_init()
//{
//	DMA_PORTx2BUFF_Init (DMA, (void *)&PTD_BYTE1_IN, temaddr, PTB21, DMA_BYTE1, 1, DMA_rising);   
//}





//void UART4_RX_TX_IRQHandler()
//{
//	u8 temp;
//	if((UART4->S1 & UART_S1_RDRF_MASK)!= 0)		//等待接收满了
//	{
//		//while (!(UART_S1_REG(UARTN[UART_4]) & UART_S1_RDRF_MASK));       //等待接收满了
//		temp = UART4->D ;
//		Data_Receive_Prepare(temp);
//	}
//}
/////////////////////////////////////////////////////////////////////////////////////
//Data_Receive_Prepare函数是协议预解析，根据协议的格式，将收到的数据进行一次格式性解析，格式正确的话再进行数据解析
//移植时，此函数应由用户根据自身使用的通信方式自行调用，比如串口每收到一字节数据，则调用此函数一次
//此函数解析出符合格式的数据帧后，会自行调用数据解析函数

void Data_Receive_Prepare(uint8 data)
{
	static uint8 RxBuffer[50];
	static uint8 _data_len = 0,_data_cnt = 0;
	static uint8 state = 0;
	
	if(state==0&&data==0xAA)
	{
		state=1;
		RxBuffer[0]=data;
	}
	else if(state==1&&data==0xAF)
	{
		state=2;
		RxBuffer[1]=data;
	}
	else if(state==2&&data<0XF1)
	{
		state=3;
		RxBuffer[2]=data;
	}
	else if(state==3&&data<50)
	{
		state = 4;
		RxBuffer[3]=data;
		_data_len = data;
		_data_cnt = 0;
	}
	else if(state==4&&_data_len>0)
	{
		_data_len--;
		RxBuffer[4+_data_cnt++]=data;
		if(_data_len==0)
		{
			state = 5;
		}
	}
	else if(state==5)
	{
		state = 0;
		RxBuffer[4+_data_cnt]=data;
		Data_Receive_Handler(RxBuffer,_data_cnt+5);
	}
	else
		state = 0;
}

/////////////////////////////////////////////////////////////////////////////////////
//Data_Receive_Anl函数是协议数据解析函数，函数参数是符合协议格式的一个数据帧，
//该函数会首先对协议数据进行校验，校验通过后对数据进行解析，实现相应功能
//此函数可以不用用户自行调用，由函数Data_Receive_Prepare自动调用
void Data_Receive_Handler(uint8 *data_buf,uint8 num)
{
	uint8 sum = 0;
	uint8 i=0;
	for(;i<(num-1);i++)
	{
		sum += *(data_buf+i);
	}
	if(!(sum==*(data_buf+num-1)))		
		return;		//判断sum，错误则返回
	if(!(*(data_buf)==0xAA && *(data_buf+1)==0xAF))		
		return;		//判断帧头，错误则返回
	
	if(*(data_buf+2)==0X02)
	{
		if(*(data_buf+4)==0X01)		//对应匿名上位机中的“读取飞控”
		{
					//开启电机
		}
		if(*(data_buf+4)==0X02)		//对应匿名上位机中的“写入飞控”
		{
			
		}
		if(*(data_buf+4)==0XA1)		//对应匿名上位机中的“恢复默认值”
		{
					//关闭电机
		}
	}
}


