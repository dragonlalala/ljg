/*
 * @Author: your name
 * @Date: 2021-05-01 19:50:44
 * @LastEditTime: 2021-05-01 20:06:16
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \gcxz\Middlewares\standard.h
 */
#ifndef _DATA_PROCESS_H
#define _DATA_PROCESS_H

template<typename T>
T constrain(T input,T min,T max){
  if (input <= min)
    return min;
  else if(input >= max)
    return max;
  else return input;
}

template<typename T>
T Get_Mid_Val(T a,T b,T c)
{
	int min,max,second;
	max= a > b ? a : b;
	max = max > c ? max : c;
	min = a < b ? a: b;
	min = min < c ? min : c;
	second = a + b + c - max - min;
	return second;
}

#endif
