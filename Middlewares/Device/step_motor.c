#include "step_motor.h"
/*初始化步进电机的端口*/
void Step_Motor_Init()
{
		gpio_init(STEP_MOTOR_DIRECTION_PIN,GPO,1);//direction
		gpio_init(STEP_MOTOT_STEP_PIN,GPO,0);     //step
		gpio_init(STEP_MOTOT_ENABLE_PIN,GPO,0);     //enable
}

/*控制步进电机停止*/
void Step_Motor_Stop()
{
		gpio_set(STEP_MOTOT_ENABLE_PIN,1);    // disable
}
	
/*控制步进电机转的圈数，x=200为一圈*/	
void Run_x_Loop(int x)
{
		int i;	
		for(i=0; i<x; i++)
		{
			//TODO 缺少向左跟向右的两个闭环反馈
			gpio_set(STEP_MOTOT_STEP_PIN,1);
			systick_delay_ms(8); 						//中间间隔的时间，频率为 1/时间
			gpio_set(STEP_MOTOT_STEP_PIN,0); 
			systick_delay_ms(8);
		}
}
