/*
 * @Author: your name
 * @Date: 2021-05-01 15:46:00
 * @LastEditTime: 2021-05-02 20:41:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \gcxz\USER\main.c
 */

#include "include_user.h"

int main(void){
	
    get_clk();//上电后必须运行一次这个函数，获取各个频率信息，便于后面各个模块的参数设置
    initAll();
	copy_chassis_target_x =  chassis.target_pose.x = 3000;
	// 现在没有考虑任何时序问题，比如OLED显示与控制周期的问题，低速下问题不大，后期优化再修改
	while(1){
		if(init_device_flag){
            init_device_flag = 0;
            initFlag();		// 初始化外设，如舵机、OLED
        }
        
		if (control_flag && permit_con_flag){           // control_flag 由周期控制，permit_con_flag由上位机控制
			control_flag  = 0;
			controlTask();
		}

		if (edgeBoard_flag){                            // edgeBoard_flag 由上位机控制
			edgeBoard_flag = 0;
			permit_con_flag = 0;
			edgeBoardTask();
		}

		if (upMonitor_flag){                            // upMonitor_flag 由周期控制
			upMonitor_flag = 0;
			upMonitorTask();
		}

		if (debug_flag){                                // debug_flag 由周期控制
			debug_flag = 0;
			debugTask();
		}	 
	}
}
