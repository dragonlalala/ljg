#ifndef _UART_DMA_H
#define _UART_DMA_H
#include "include_lib.h"

void initUart_DMA(UART_Type* uart);
void sendUart_DMA(UART_Type* uart, uint8* data,int16 count);

#endif

