# 16th BaiDu AI - K66



## 项目描述



## TODO

- [ ] 学习上位机使用
- [ ] 增加ProcessOn流程图，并添加协作者
- [ ] 时钟似乎快了一倍：进debug看sys_time，会比现实时间快一倍
- [ ] 按照chassis/motor风格封装arm/steering
- [ ] 修改Middleware/Device文件规范：用类封装



## 文件说明

```c++
├─ USER
│  ├─ main.c
│  ├─ isr.c 中断服务函数
│  ├─ config.cpp 初始化
│  ├─ task.cpp 任务
│  ├─ data.cpp 放置所有的全局变量
│  ├─ include_user.h
│  ├─ include_mid.h
│  └─ include_lib.h
│    
├─ Middlewares 中间层
│  ├─ Algorithm 算法
│  │  ├─ pid.cpp
│  │  └─ filters.cpp
│  ├─ Communicate 通信
│  │  ├─ UpperMonitor 上位机通信
│  │  └─ ano_upper_monitor.c edgeboard通信
│  └─ Device
│  │  ├─ chassis.cpp 底盘
│  │  └─ motor.cpp 电机
│  ├─ Mydrives 对芯片库进行二次封装，或者提供芯片库没有的功能
│  │  ├─ dma.c
│  │  └─ uart_dma.c
│  └─ Utils 工具
│     ├─ data_prcess.h 数据处理函数，比如constraint
│     └─ standard.h 一些约定的define或者结构体等
│
├─ Libraries 芯片库和外设库
│
└─ .md
   ├─ README.md
   ├─ style.md 编程风格、规范
   ├─ tip.md 编程技巧、经验
   └─ version.md 更新记录
```



## 约定

1、底盘坐标系

前向为X轴正方向，左向为Y轴正方向，逆时针为Z正方向（遵循右手坐标系）

2、下标 

电机：`LF：0`，`RF：1`，`RB：2`，`LB：3`

姿态： `X：0`，`Y：1`，`Z：3`



## 注意

1、在isr.c中添加**中断服务函数**时，需要在isr.h中的**extern"C"**内声明函数



