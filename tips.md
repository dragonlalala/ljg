# Coding Tips

Document For Recording Coding Experience



+ 使用abstract class、.h头文件约定接口
+ .c中定义，.h中extern
+ 只有在某个函数中用到的静态变量，就在函数中使用static定义，不要定义全局变量
+ 数据处理之类的函数应该放在utils文件夹中
+ 应该inlcude尽可能少的文件（只包含必要的文件）
+ 对于软件架构设计，如果不知道要采用什么架构，三层架构是一个很不错的选择，比如智能车的程序结构
+ 阅读源码时，从main开始，分任务阅读

