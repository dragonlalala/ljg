/*
 * @Author: your name
 * @Date: 2021-05-02 12:41:39
 * @LastEditTime: 2021-05-06 16:35:30
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \gcxz\USER\data.c
 */

#include "data.h"

uint32 sys_time_ms = 0;

int8 control_flag = 0;		 // 控制任务
int8 edgeBoard_flag = 0;	 // edgeBoard通信，包括发送与接收
int8 upMonitor_flag = 0;	 // 上位机通信，包括发送与接收
int8 init_device_flag = 0;   // 初始化舵机举旗等的初始位置
int8 debug_flag = 0;		 // 调试任务，包括OLED显示、key设置等
int8 couandstop_flag = 0;	 // 计步标志位
int8 task_flag = 0;          // 下位机做任务的标志位
int8 adjust_flag = 0;        // 下位机进行调整的标志位
int8 permit_con_flag = 0;    // 上位机控制要不要下位机进行底盘电机控制 
uint8 task_type = 0;		 // 下位机接收到的任务类型
short which_city;            // 接收上位机传来的哪座城池
int8 eb_dubug_flag = 0;      // 接收上位机传来的要改变下位机变量的标志位
uint8 argument_value = 0;    // 上位机发来的要修改的哪个变量的值
uint8 which_argument = 0;    // 上位机发来的要修改的哪个变量
Chassis chassis;             // 定义一个底盘

int RB_cnt = 0;
int LB_cnt = 0;
