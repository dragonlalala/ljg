#ifndef _KEY_H
#define _KEY_H
#include "include_lib.h"
#define get_key  gpio_get(KEY1_PIN)*1+gpio_get(KEY2_PIN)*2+gpio_get(KEY3_PIN)*3  //获取按键情况
#define KEY1  				1   
#define KEY2  				2 
#define KEY3  				3
#define NONE				  6
extern uint8 key_interrupt_flag;//按键中断标志位
extern uint8 key_value_last;//上一个键值
extern uint8 key_value;//当前键值

void Key_Init(void);//按键初始化
uint8 Key_Select(void);//按键判断函数
#endif


