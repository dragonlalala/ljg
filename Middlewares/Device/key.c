#include "key.h"

uint8 key_interrupt_flag;//按键中断标志位
uint8 key_value_last;//上一个键值
uint8 key_value;//当前键值

void Key_Init(void)//按键初始化
{
	port_init (KEY1_PIN, IRQ_EITHER | PF | ALT1 | PULLUP );	//初始化 D1 管脚，跳变沿触发中断，带无源滤波器，复用功能为GPIO并设置为输入 ，上拉电阻
	port_init (KEY2_PIN, IRQ_EITHER | PF | ALT1 | PULLUP );
	port_init (KEY3_PIN, IRQ_EITHER | PF | ALT1 | PULLUP );
	set_irq_priority(PORTD_IRQn,2);						    //设置优先级
	enable_irq(PORTD_IRQn);								    //打开PORTD中断开关
	EnableInterrupts;									    //打开总的中断开关

	key_value=6;											//啥都不按的情况，本来应该怎样，但是好像硬件有问题，没按的时候是5
	key_interrupt_flag=0;									//按键中断标志位，在多个文件传递
}

uint8 Key_Select(void)
{
	
	if(key_interrupt_flag==1)
	{

		key_interrupt_flag=0;								//清除按键按下的标志
		return key_value;
//		switch(key_value)
//		{			
//			case 5: break;                     //从右边开始数第一个按键
//			case 4: break;                     //从右边开始数第二个按键
//			case 3: break;                     //从右边开始数第三个按键
//			default: break;
//		}		
	}
	else{
		return 6;
	}
}
