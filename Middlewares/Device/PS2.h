#ifndef __PS2_H
#define __PS2_H

/*********************************************************
	@layor			Components模块层		  
	@brief     		模拟SPI协议，PS2遥控模块
	@pin			通用输入  DI/DAT；通用输出  DO/CMD   CS  CLK
	@Organization 	Scut_SmartCar
	@Author			xjn(qq765760800)
	@version    	21/1/18	v1.0 完成了初步的封装，把最常用的功能的封装的，默认：可调整按键模式或摇杆模式、手柄震动不开启

	@Attention		
	@用法			
	@用法举例   
				int main(void)
				{
					//此处省略其他初始化
					
					systick_delay_ms(1000);
					PS2_Init();			//=====ps2相关MCU底层初始化以及ps2功能配置
					systick_delay_ms(1000);
					
					while(1)
					{
						//获取手柄信息（主循环中）
						PS2_LX=PS2_AnologData(PSS_LX);
						PS2_LY=PS2_AnologData(PSS_LY);
						PS2_RX=PS2_AnologData(PSS_RX);
						PS2_RY=PS2_AnologData(PSS_RY);
						PS2_KEY=PS2_DataKey();
						
						//打印手柄信息
						printf("PS2_LX: %d    ",PS2_LX);
							systick_delay_ms(100);
						printf("PS2_LY: %d    ",PS2_LY);		
							systick_delay_ms(100);
						printf("PS2_RX: %d    ",PS2_RX);
							systick_delay_ms(100);
						printf("PS2_RY: %d    ",PS2_RY);
							systick_delay_ms(100);
						printf("PS2_KEY:%d\r\n",PS2_KEY);
							systick_delay_ms(100);	
					}
				}
**********************************************************/	



/******************************	具体应用信息******************************/

//---------------------------------------------
//当前接口的用途：霄润君华、嵩江队1号车(三轮)    			//用于标记这个模块用于谁的、怎样的实物（可以删除这句注释）
//---------------------------------------------
//当前代码所需物理连接方式与引脚配置：
//适用模块实物：市面上的某种六脚ps2手柄接收器。引脚顺序是：GND - VDD(3.3~5) - DI/DAT - DO/CMD - CS - CLK
//适用核心板：LQ-K66P144-SYS VG
//			  如果使用的主控板是车队的培训板，那么可以将接收器插在主控板的LED接口排母上。
//			  上电前切记根据实物PCB丝印或PCB文件检查引脚是否接对！
//通用输入：  DI/DAT -> B11	
//通用输出：  DO/CMD -> B16 
//			  CS	 -> B9	
//			  CLK	 -> B8	
//---------------------------------------------



//-------------------------------------------宏定义修改区Begin-by辛君诺-------------------------------------------//


//头文件大全集
#include "include_lib.h"


////以下为引脚设置宏定义，此处使用了位带操作
//#define DI		PBin(11)		 //DI输入

//#define DO_H 	PBout(16)=1       //命令位拉高
//#define DO_L 	PBout(16)=0		//命令位拉低

//#define CS_H 	PBout(9)=1			//CS拉高
//#define CS_L 	PBout(9)=0       //CS拉低

//#define CLK_H 	PBout(8)=1      //时钟拉高
//#define CLK_L 	PBout(8)=0      //时钟拉低


//以下为us级延时函数宏定义
#define __DELAY_US(__NUMBER_OF_US__)			\
	do{											\
		systick_delay_us(__NUMBER_OF_US__);		\
	}while(0)


	
///******以下为引脚初始化函数宏定义******/
////CLK引脚初始化函数
//#define __PS2_CLK_PIN_INIT				\
//	do{									\
//		gpio_init(B8,GPO,1);			\
//	}while(0)
//	
////CS引脚初始化函数
//#define __PS2_CS_PIN_INIT				\
//	do{									\
//		gpio_init(B9,GPO,1);			\
//	}while(0)
//	
////DO引脚初始化函数
//#define __PS2_DO_PIN_INIT				\
//	do{									\
//		gpio_init(B16,GPO,1);			\
//	}while(0)
//	
////DI引脚初始化函数
//#define __PS2_DI_PIN_INIT				\
//	do{									\
//		gpio_init(B11,GPI,0);			\
//	}while(0)

//-------------------------------------by 刘洁耿------------------------------
	//通用输入：  DI/DAT -> B11	
//通用输出：  DO/CMD -> B16 
//			  CS	 -> B9	
//			  CLK	 -> B8	

	//以下为引脚设置宏定义，此处使用了位带操作
#define DI		PAin(8)		 //DI输入

#define DO_H 	PAout(9)=1       //命令位拉高
#define DO_L 	PAout(9)=0		//命令位拉低

#define CS_H 	PAout(10)=1			//CS拉高
#define CS_L 	PAout(10)=0       //CS拉低

#define CLK_H 	PAout(11)=1      //时钟拉高
#define CLK_L 	PAout(11)=0      //时钟拉低


//以下为us级延时函数宏定义
#define __DELAY_US(__NUMBER_OF_US__)			\
	do{											\
		systick_delay_us(__NUMBER_OF_US__);		\
	}while(0)


	
/******以下为引脚初始化函数宏定义******/
//CLK引脚初始化函数
#define __PS2_CLK_PIN_INIT				\
	do{									\
		gpio_init(A11,GPO,1);			\
	}while(0)
	
//CS引脚初始化函数
#define __PS2_CS_PIN_INIT				\
	do{									\
		gpio_init(A10,GPO,1);			\
	}while(0)
	
//DO引脚初始化函数
#define __PS2_DO_PIN_INIT				\
	do{									\
		gpio_init(A9,GPO,1);			\
	}while(0)
	
//DI引脚初始化函数
#define __PS2_DI_PIN_INIT				\
	do{									\
		gpio_init(A8,GPI,0);			\
	}while(0)


//--------------------------------------------宏定义修改区End--------------------------------------------//


	
	
extern int 	PS2_LX,  /*左摇杆x轴*/ 	\
			PS2_LY,  /*左摇杆y轴*/ 	\
			PS2_RX,  /*右摇杆x轴*/ 	\
			PS2_RY,  /*右摇杆y轴*/ 	\
			PS2_KEY; /*按键号*/
	
//以下为固定延时5us函数
#define __DELAY_5US  __DELAY_US(5)


//These are our button constants
#define PSB_SELECT      1
#define PSB_L3          2
#define PSB_R3          3
#define PSB_START       4
#define PSB_PAD_UP      5
#define PSB_PAD_RIGHT   6
#define PSB_PAD_DOWN    7
#define PSB_PAD_LEFT    8
#define PSB_L2          9
#define PSB_R2          10
#define PSB_L1          11
#define PSB_R1          12
#define PSB_GREEN       13
#define PSB_RED         14
#define PSB_BLUE        15
#define PSB_PINK        16

#define PSB_TRIANGLE    13
#define PSB_CIRCLE      14
#define PSB_CROSS       15
#define PSB_SQUARE      16

//#define WHAMMY_BAR		8

//These are stick values
#define PSS_RX 5                //右摇杆X轴数据
#define PSS_RY 6
#define PSS_LX 7
#define PSS_LY 8

extern uint8 Data[9];
extern uint16 MASK[16];
extern uint16	 Handkey;

void PS2_Init(void);
uint8 PS2_RedLight(void);   //判断是否为红灯模式
void PS2_ReadData(void); //读手柄数据
void PS2_Cmd(uint8 CMD);		  //向手柄发送命令
uint8 PS2_DataKey(void);		  //按键值读取
uint8 PS2_AnologData(uint8 button); //得到一个摇杆的模拟量
void PS2_ClearData(void);	  //清除数据缓冲区
void PS2_Vibration(uint8 motor1, uint8 motor2);//振动设置motor1  0xFF开，其他关，motor2  0x40~0xFF

void PS2_EnterConfing(void);	 //进入配置
void PS2_TurnOnAnalogMode(void); //发送模拟量
void PS2_VibrationMode(void);    //振动设置
void PS2_ExitConfing(void);	     //完成配置
void PS2_MSP_Init(void);		//MCU底层初始化
void PS2_SetInit(void);		     //配置初始化
void PS2_Motor_Control(void);
#endif





